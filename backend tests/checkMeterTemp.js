/**********************
* Script for checking the the ambient temperature being measured by the meter. This script will check the
* temperature recursively on a time delay
* Takes in the user JSON, location name, time delay in seconds, and number of loops as command line arguments. I.e. "node checkMeterTemp.js myUser.json myHouse 30 60"
**********************/

//import modules
var apiLib = require("./RWC_API_lib.js");
var fs = require('fs');

//get command line arguments
var userFile = "";
var locationName = "";
var delayMilli = 0;
var numRuns = 0;//number of times to run the temp check
if (process.argv.length < 6){
	console.log("Please pass a user JSON, location name, time delay in seconds, and number of loops as an argumenst to this script. For example:\n   node checkMeterTemp.js myUser.json myHouse 30 60");
	process.exit(1);
}else{
	userFile = process.argv[2];
	locationName = process.argv[3];
	delayMilli = process.argv[4] * 1000;
	numRuns = process.argv[5];
}

var envJSON;
var userJSON;
//read in environment and user data from JSON files
try{
	envJSON = JSON.parse(fs.readFileSync('environment.json', 'utf8'));
	userJSON = JSON.parse(fs.readFileSync(userFile, 'utf8'));
}
catch(e){
	console.log("Error parsing JSON: " + e);
	process.exit(1);
}



//authenticate the user
apiLib.userAuth(envJSON, userJSON, function callback(success, err){
	if(success){//if authentication was successful
		
		//recursive promise function for running the temperature checks
		var checkTemp = function(deviceID, count){
			return new Promise(function (resolve, reject) {//the promise to return
				if(count > numRuns){
					resolve("done");
				}
				else{
					//used to call the promise resolve from the timeout
					function callResolve(){
						resolve(checkTemp(deviceID, count+1))
					}
					
					apiLib.getDevice(deviceID).then(function (results) {
						if ("code" in results){//if the API call returned an error
							console.log(results.message);
							var rejectMsg = "Error getting device. Error code: " + results.code + "\nMessage: " + results.message + "\nErrors: " + results.errors;
							reject(rejectMsg);
						}
						else{
							var now = new Date();
							console.log(now.toString() + " --> Current temp: " + results.temperature);
							//wait 15 seconds, then make recursive resolve call
							setTimeout(callResolve, delayMilli);
						}
					});
				}
			});
		}
		
		apiLib.getAllLocations()
		.then(function(res){
			if("code" in res){
				console.log("Error getting all locations");
				console.log(res);
			}
			else{
				var locations = res.locations;
				var deviceID = "";
				for(i=0; i<locations.length; i++){
					if(locationName == locations[i].name){
						deviceID = locations[i].device.deviceId;
					}
				}
				if(deviceID == ""){
					console.log(locationName + " was not in the locations list.");
					process.exit(1);
				}
				else{
					checkTemp(deviceID, 1).then(console.log, console.log);
				}
			}
		});
		//run recursive function
	}
	else{//if authentication failed
		console.log(err);
	}
});