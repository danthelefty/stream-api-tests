/**********************
 * Script for getting the list of all locations for an account.
 * Takes in a command line argument for the path to the userJSON file. I.e. "node getAllLocationsForAccount.js user.json"
**********************/

//import modules
var apiLib = require("./RWC_API_lib.js");
var fs = require('fs');

var userFile = "";
if (process.argv.length <= 2){
	console.log("Please pass a user JSON file as an argument to this script. For example:\n   node getAllLocationsForAccount.js user.json");
	process.exit(1);
}else{
	userFile = process.argv[2];
}

//read in environment and user data from JSON files
try{
var envJSON = JSON.parse(fs.readFileSync('environment.json', 'utf8'));
var userJSON = JSON.parse(fs.readFileSync(userFile, 'utf8'));
}
catch(e){
	console.log("Error parsing JSON: " + e);
	process.exit(1);
}

//authenticate the user
apiLib.userAuth(envJSON, userJSON, function callback(success, err){
	if(success){//if authentication was successful
		console.log("Getting locations...");
		apiLib.getAllLocations()
			.then(function (results){
				console.log(JSON.stringify(results, null, 3));
			});
	}
	else{
		console.log(err);
	}
});