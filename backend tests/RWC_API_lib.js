/*********************
 * Library of all API calls for the Stream backend.
 * This is a module to be imported into other node.js test scripts. While the userAuth function uses a callback,
 * all other functions use promises to handle their asynchronous responses.
*********************/

//import modules
const AWS = require('aws-sdk');
var AmazonCognitoIdentity = require('amazon-cognito-identity-js');
var apigClientFactory = require('aws-api-gateway-client');


//global variables
var identityId;
var expiration;
var apigClient;


/*********************
* Fix issues with cognito identity
* module that requires Browser DOM
*********************/
var LocalStorage = require('node-localstorage').LocalStorage;
localStorage = new LocalStorage('./scratch');

global.window = {
  localStorage: {
    getItem() {
      return null;
    },
    setItem() {},
    deleteItem() {}
  }
};
global.navigator = {};
/**********************/


/*********************
 * Function for authenticating user in AWS. This function must be ran before any other requests can be made.
 * This function first makes the call for authenticating the user, then configures the aws-api-gateway-client
 * with the info returned by the authentication call. This aws-api-gateway-client is then used for making all
 * subsiquent API calls.
 *
 * envJSON: Object containing the environment data used for authentication and gateway client. This object should have the following form:
 *			{
 *				"region": "us-east-1",
 *				"userPoolId": "us-east-1_tgwIZ5b1b",
 *				"identityPoolId": "us-east-1:0bc58e42-6191-4bfe-8a13-df3e8d005146",
 *				"clientId": "14d2kqfkt3vr2rj4sntnm0jm63",
 *				"accountId": "668156139991",
 *				"invokeUrl": "https://dev-api.streemlabs.io"
 *			}
 *
 * userJSON: Object containing the user data used for authentication and gateway client. This object should have the following form:
 *			{
 *				"roleArn": "arn:aws:iam::668156139991:role/Cognito_DevFederatedAuth_Role",
 *				"Username": "jenkins",
 *				"Password": "7nf9*4ByAG06R92Z"
 *			}
 *
 *callback: the callback function that returns the asynchronous response. Takes the form of callback({boolean} success[, {Object} err]}
 *********************/
exports.userAuth = function(envJSON, userJSON, callback){
	console.log("Authenticating user '" + userJSON.Username + "'...");
	
	//setting up values for authentication...
	var authData = {
		Username: userJSON.Username,
		Password: userJSON.Password
	};
	var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authData);
	
	var poolData = {
		UserPoolId: envJSON.userPoolId,
		ClientId: envJSON.clientId
	};
	var userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
	
	var userData = {
		Username: userJSON.Username,
		Pool: userPool
	};
	var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
	
	//run user authentication
	cognitoUser.authenticateUser(authenticationDetails, {
		onSuccess: function (result) {//callback for successful authentication
			//console.log(result);
			console.log("User authentication successful! Logged in as: " + userJSON.Username);
			
			//config AWS with this user and environment
			AWS.config.region = envJSON.region;
			AWS.config.credentials = new AWS.CognitoIdentityCredentials({
				IdentityPoolId: envJSON.identityPoolId, // your identity pool id here
				RoleArn: userJSON.roleArn,
				AccountId: envJSON.accountId
			});
			AWS.config.credentials.params.Logins = {};
			AWS.config.credentials.params.Logins['cognito-idp.' + envJSON.region + '.amazonaws.com/' + envJSON.userPoolId] = result.getIdToken().getJwtToken();
			
			//refress to get credentials
			AWS.config.credentials.refresh(function (err) {
				if (err) {
					console.log('Error in authenticating to AWS.');
					callback(false, err);
				} else {
					identityId = AWS.config.credentials.identityId;
					var accessKeyId = AWS.config.credentials.accessKeyId;
					var secretAccessKey = AWS.config.credentials.secretAccessKey;
					var sessionToken = AWS.config.credentials.sessionToken;
					expiration = AWS.config.credentials.expiration;
					
					var apigConfig = {
						invokeUrl: envJSON.invokeUrl,
						accessKey: accessKeyId,
						secretKey: secretAccessKey,
						sessionToken: sessionToken,
						region: envJSON.region
					};
					apigClient = apigClientFactory.newClient(apigConfig);
					callback(true);
				}
			});
		},
		onFailure: function (err) {//callback for failed authentication
			callback(false, err);
		},
		mfaRequired: function (codeDeliveryDetails) {//callback for when MFA is required for authentication
			// MFA is required to complete user authentication.
			// Get the code from user and call
			cognitoUser.sendMFACode(mfaCode, this);
		},

		newPasswordRequired: function (userAttributes, requiredAttributes) {//callback for when a new password is required (i.e. first time login)
			console.log(userAttributes);
			console.log(requiredAttributes);
			// User was signed up by an admin and must provide new
			// password and required attributes, if any, to complete
			// authentication.

			// Get these details and call
			var newPassword = userJSON.Password;
			var attributesData = {
			  'name': userJSON.Username,
			  'preferred_username': userJSON.Username
			};
			cognitoUser.completeNewPasswordChallenge(newPassword, attributesData, this);
		}
	});
};//end userAuth()


/*********************
 * Run the GET device API call. Sends back a promise containing either the https response data or error message.
 *
 * deviceID: The ID of the meter to call
*********************/
exports.getDevice = function(deviceID){
	var params = {};
    var pathTemplate = '/v1/devices/' + deviceID;
    var method = 'GET';
    var additionalParams = {};
    var body = {};
	
	return new Promise(function (resolve) {
		apigClient.invokeApi(params, pathTemplate, method, additionalParams, body)
			.then(function (result) {
				resolve(result.data);
			}).catch(function (result) {
				resolve(result.response.data);
			});
	});
};//end getDevice()



/*********************
 * Run the POST device API call. Sends back a promise containing either the https response data or error message.
 *
 * locationID: the ID of the location to post the meter to
 * deviceID: the ID of the meter to post
*********************/
exports.postDevice = function(locationID, deviceID){
	var params = {};
    var pathTemplate = '/v1/devices';
    var method = 'POST';
    var additionalParams = {};
    var body = {
		"locationId": locationID,
		"deviceId": deviceID
	};
	return new Promise(function (resolve) {
		apigClient.invokeApi(params, pathTemplate, method, additionalParams, body)
			.then(function (result) {
				resolve(result.data);
			}).catch(function (result) {
				resolve(result.response.data);
			});
	});
}//end postDevice()


/*********************
 * Run the POST new location API call. Sends back a promise containing either the https response data or error message.
 *
 * locationName: name of the location to create
 * timezone: the numeric timezone of this location
 * zipcode: (optional) string zip code for the location
 * buildingType: (optional) string building type for the location
*********************/
exports.postLocation = function(locationName, timezone, zipcode = "", buildingType = ""){
	var params = {};
    var pathTemplate = '/v1/locations';
    var method = 'POST';
    var additionalParams = {};
    var body = {
		"name": locationName,
		"timezone": timezone
	};
	if (zipcode != ""){
		body.zip = zipcode;
	}
	if (buildingType != ""){
		body.buildingType = buildingType;
	}
	
    return new Promise(function (resolve) {
		apigClient.invokeApi(params, pathTemplate, method, additionalParams, body)
			.then(function (result) {
				resolve(result.data);
			}).catch(function (result) {
				resolve(result.response.data);
			});
	});
}//end postLocation()


/*********************
 * Run the GET all locations API call. Sends back a promise containing either the https response data or error message.
*********************/
exports.getAllLocations = function(){
	var params = {};
    var pathTemplate = '/v1/locations';
    var method = 'GET';
    var additionalParams = {};
    var body = {};
	
    return new Promise(function (resolve) {
		apigClient.invokeApi(params, pathTemplate, method, additionalParams, body)
			.then(function (result) {
				resolve(result.data);
			}).catch(function (result) {
				resolve(result.response.data);
			});
	});
}//end getAllLocations()


/*********************
 * Run the GET a location API call. Sends back a promise containing either the https response data or error message.
 *
 * locationID: the ID of the location to post the meter to
*********************/
exports.getLocation = function(locationID){
	var params = {};
    var pathTemplate = '/v1/locations/'+locationID;
    var method = 'GET';
    var additionalParams = {};
    var body = {};
	
    return new Promise(function (resolve) {
		apigClient.invokeApi(params, pathTemplate, method, additionalParams, body)
			.then(function (result) {
				resolve(result.data);
			}).catch(function (result) {
				resolve(result.response.data);
			});
	});
}//end getLocation()


/*********************
 * Run the PUT a location API call. Sends back a promise containing either the https response data or error message.
 *
 * locationID: the ID of the location to post the meter to
 * locationName: name of the location to create
 * timezone: the numeric timezone of this location
 * zipcode: (optional) string zip code for the location
 * buildingType: (optional) string building type for the location
*********************/
exports.putLocation = function(locationID, locationName, timezone, zipcode = "", buildingType = ""){
	var params = {};
    var pathTemplate = '/v1/locations/'+locationID;
    var method = 'PUT';
    var additionalParams = {};
    var body = {
		"name": locationName,
		"timezone": timezone
	};
	
	if (zipcode != ""){
		body.zip = zipcode;
	}
	if (buildingType != ""){
		body.buildingType = buildingType;
	}
	
    return new Promise(function (resolve) {
		apigClient.invokeApi(params, pathTemplate, method, additionalParams, body)
			.then(function (result) {
				resolve(result.data);
			}).catch(function (result) {
				resolve(result.response.data);
			});
	});
}//end putLocation()


/*********************
 * Run the GET location trends API call. Sends back a promise containing either the https response data or error message.
 *
 * locationID: the ID of the location
 * timeZone: numeric UTC offset value
*********************/
exports.getLocationTrends = function(locationID, timeZone){
	var params = {};
    var pathTemplate = '/v1/locations/'+locationID + '/trends';
    var method = 'GET';
    var additionalParams = {
		headers: {
			"utc-offset": timeZone
		}
	};
    var body = {};
	
    return new Promise(function (resolve) {
		apigClient.invokeApi(params, pathTemplate, method, additionalParams, body)
			.then(function (result) {
				resolve(result.data);
			}).catch(function (result) {
				resolve(result.response.data);
			});
	});
}//end getLocationTrends()


/*********************
 * Run the DELETE a location API call. Sends back a promise containing either the https response data or error message.
 *
 * locationID: the ID of the location
*********************/
exports.deleteLocation = function(locationID){
	var params = {};
    var pathTemplate = '/v1/locations/'+locationID;
    var method = 'DELETE';
    var additionalParams = {};
    var body = {};
	
    return new Promise(function (resolve) {
		apigClient.invokeApi(params, pathTemplate, method, additionalParams, body)
			.then(function (result) {
				resolve(result.data);
			}).catch(function (result) {
				resolve(result.response.data);
			});
	});
}//end deleteLocation()


/*********************
 * Run the DELETE device API call. Sends back a promise containing either the https response data or error message.
 *
 * deviceID: The ID of the meter to call
*********************/
exports.deleteDevice = function(deviceID){
	var params = {};
    var pathTemplate = '/v1/devices/' + deviceID;
    var method = 'DELETE';
    var additionalParams = {};
    var body = {};
	
	return new Promise(function (resolve) {
		apigClient.invokeApi(params, pathTemplate, method, additionalParams, body)
			.then(function (result) {
				resolve(result.data);
			}).catch(function (result) {
				resolve(result.response.data);
			});
	});
};//end deleteDevice()


/*********************
 * Run the  API call. Sends back a promise containing either the https response data or error message.
 *
 * platform: the platform of the mobile device ("iOS" or "Android")
 * token: id token for the mobile device
*********************/
exports.createMoblePushNotification = function(platform, token){
	var params = {};
    var pathTemplate = '/v1/mobile/push';
    var method = 'POST';
    var additionalParams = {};
    var body = {
		"platform": platform,
		"token": token
	};
	
	return new Promise(function (resolve) {
		apigClient.invokeApi(params, pathTemplate, method, additionalParams, body)
			.then(function (result) {
				resolve(result.data);
			}).catch(function (result) {
				resolve(result.response.data);
			});
	});
};//end createMoblePushNotification()


/*********************
 * Run the send command to device API call. Sends back a promise containing either the https response data or error message.
 *
 * deviceID: The ID of the meter to call
 * cmd: the JSON for the command to call
*********************/
exports.sendCommandToDevice = function(deviceID, cmd){
	var params = {};
    var pathTemplate = '/v1/devices/' + deviceID + '/command';
    var method = 'POST';
    var additionalParams = {};
    var body = cmd;
	
	return new Promise(function (resolve) {
		apigClient.invokeApi(params, pathTemplate, method, additionalParams, body)
			.then(function (result) {
				resolve(result.data);
			}).catch(function (result) {
				resolve(result.response.data);
			});
	});
};//end sendCommandToDevice()