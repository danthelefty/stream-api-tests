/**********************
 * Script for updating a location using the PUT location command.
 * Takes in the user JSON, location name, new location name, numeric timezone, zip code, and building type as command line arguments.
 * I.e. 'node updateLocation.js myUser.json locationName newLocationName -4 "12345" office'
 * zip code and building type are optional
**********************/

//import modules
var apiLib = require("./RWC_API_lib.js");
var fs = require('fs');

//get command line arguments
var userFile = "";
var locationName = "";
var newLocationName = "";
var timezone = 0;
var zipcode = "";
var buildingType = "";

if (process.argv.length < 6){
	console.log('Please pass a user JSON file and location name as an argumenst to this script. For example:\n   node updateLocation.js myUser.json locationName newLocationName -4 "12345" office');
	process.exit(1);
}else{
	userFile = process.argv[2];
	locationName = process.argv[3];
	newLocationName = process.argv[4];
	timezone = process.argv[5];
	if(process.argv.length >= 7){
		zipcode = process.argv[6];
	}
	if(process.argv.length >= 8){
		buildingType = process.argv[7];
	}
}

//read in environment and user data from JSON files
try{
var envJSON = JSON.parse(fs.readFileSync('environment.json', 'utf8'));
var userJSON = JSON.parse(fs.readFileSync(userFile, 'utf8'));
}
catch(e){
	console.log("Error parsing JSON: " + e);
	process.exit(1);
}

//authenticate the user
apiLib.userAuth(envJSON, userJSON, function callback(success, err){
	if(success){//if authentication was successful
		apiLib.getAllLocations()
		.then(function(res){
			if("code" in res){//if error getting all locations, report error and exit
				console.log("Error getting all locations");
				console.log(res);
				process.exit(1);
			}
			else{//otherwise, loop through results to find desired locationID for the location name
				var locations = res.locations;
				var locationID = "";
				for(i=0; i<locations.length; i++){
					if(locationName == locations[i].name){
						locationID = locations[i].locationId;
					}
				}
				if(locationID == "" || locationID == undefined ){//if location name not in the results, report error and exit
					console.log(locationName + " was not in the locations list.");
					process.exit(1);
				}
				else{//if device was found for location name, run DELETE device
					console.log("Updating location info...");
					apiLib.putLocation(locationID, newLocationName, timezone, zipcode, buildingType)
					.then(function(result){
						console.log(result);
					});
				}
			}
		});
	}
});