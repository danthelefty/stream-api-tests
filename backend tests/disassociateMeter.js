/**********************
 * Script for disassociating a meter from a user's account.
 * Takes in the user JSON and location name as command line arguments. I.e. "node disassociateMeter.js myUser.json myHouse"
**********************/

//import modules
var apiLib = require("./RWC_API_lib.js");
var fs = require('fs');

//get command line arguments
var userFile = "";
var locationName = "";
if (process.argv.length < 4){
	console.log("Please pass a user JSON file and location name as an argumenst to this script. For example:\n   node disassociateMeter.js myUser.json myHouse");
	process.exit(1);
}else{
	userFile = process.argv[2];
	locationName = process.argv[3];
}

//read in environment and user data from JSON files
try{
var envJSON = JSON.parse(fs.readFileSync('environment.json', 'utf8'));
var userJSON = JSON.parse(fs.readFileSync(userFile, 'utf8'));
}
catch(e){
	console.log("Error parsing JSON: " + e);
	process.exit(1);
}

//authenticate the user
apiLib.userAuth(envJSON, userJSON, function callback(success, err){
	if(success){//if authentication was successful
		apiLib.getAllLocations()
		.then(function(res){
			if("code" in res){//if error getting all locations, report error and exit
				console.log("Error getting all locations");
				console.log(res);
				process.exit(1);
			}
			else{//otherwise, loop through results to find desired device by location name
				var locations = res.locations;
				var deviceID = "";
				for(i=0; i<locations.length; i++){
					if(locationName == locations[i].name && locations[i].device != null){
						deviceID = locations[i].device.deviceId;
					}
				}
				if(deviceID == "" || deviceID == undefined ){//if location name not in the results, report error and exit
					console.log(locationName + " was not in the locations list.");
					process.exit(1);
				}
				else{//if device was found for location name, run DELETE device
					console.log("deleting device from location...");
					apiLib.deleteDevice(deviceID)
					.then(function(result){
						console.log(result);
					});
				}
			}
		});
	}
});