/**********************
 * Script for getting the location trends for a named location.
 * Takes in the user JSON, location name, and UTC time zone as command line arguments. I.e. "node locationTrends.js myUser.json myHouse -5"
**********************/

//import modules
var apiLib = require("./RWC_API_lib.js");
var fs = require('fs');

//get command line arguments
var userFile = "";
var locationName = "";
var timeZone = 0;
if (process.argv.length < 5){
	console.log("Please pass a user JSON file, location name, and timezone offset as an argumenst to this script. For example:\n   node locationTrends.js myUser.json myHouse -5");
	process.exit(1);
}else{
	userFile = process.argv[2];
	locationName = process.argv[3];
	timeZone = process.argv[4];
}

//read in environment and user data from JSON files
try{
var envJSON = JSON.parse(fs.readFileSync('environment.json', 'utf8'));
var userJSON = JSON.parse(fs.readFileSync(userFile, 'utf8'));
}
catch(e){
	console.log("Error parsing JSON: " + e);
	process.exit(1);
}

//authenticate the user
apiLib.userAuth(envJSON, userJSON, function callback(success, err){
	if(success){//if authentication was successful
		apiLib.getAllLocations()
		.then(function(res){
			if("code" in res){//if error getting all locations, report error and exit
				console.log("Error getting all locations");
				console.log(res);
				process.exit(1);
			}
			else{//otherwise, loop through results to find desired locationID for the location name
				var locations = res.locations;
				var locationID = "";
				for(i=0; i<locations.length; i++){
					if(locationName == locations[i].name){
						locationID = locations[i].locationId;
					}
				}
				if(locationID == "" || locationID == undefined ){//if location name not in the results, report error and exit
					console.log(locationName + " was not in the locations list.");
					process.exit(1);
				}
				else{//if device was found for location name, show results
					console.log("Fetching location trends...");
					apiLib.getLocationTrends(locationID, timeZone)
					.then(function(result){
						console.log(result);
						
						var dailyTotal = 0;
						for(var i=0; i<result.day.length; i++){
							var dailyTotal = dailyTotal + result.day[i];
						}
						console.log("Daily total: " + dailyTotal);
						
						var monthTotal = 0;
						for(var i=0; i<result.month.length; i++){
							var monthTotal = monthTotal + result.month[i];
						}
						console.log("Month total: " + monthTotal);
						
						var yearlyTotal = 0;
						for(var i=0; i<result.year.length; i++){
							var yearlyTotal = yearlyTotal + result.year[i];
						}
						console.log("Yearly total: " + yearlyTotal);
					});
				}
			}
		});
	}
});