/**********************
 * Script for deleting a location.
 * Takes in the user JSON and location name as command line arguments.
 * I.e. 'node deleteLocation.js myUser.json locationName'
**********************/

//import modules
var apiLib = require("./RWC_API_lib.js");
var fs = require('fs');

//get command line arguments
var userFile = "";
var locationName = "";

if (process.argv.length < 4){
	console.log('Please pass a user JSON file and location name as an argumenst to this script. For example:\n   node deleteLocation.js myUser.json locationName');
	process.exit(1);
}else{
	userFile = process.argv[2];
	locationName = process.argv[3];
}

//read in environment and user data from JSON files
try{
var envJSON = JSON.parse(fs.readFileSync('environment.json', 'utf8'));
var userJSON = JSON.parse(fs.readFileSync(userFile, 'utf8'));
}
catch(e){
	console.log("Error parsing JSON: " + e);
	process.exit(1);
}

//authenticate the user
apiLib.userAuth(envJSON, userJSON, function callback(success, err){
	if(success){//if authentication was successful
		apiLib.getAllLocations()
		.then(function(res){
			if("code" in res){//if error getting all locations, report error and exit
				console.log("Error getting all locations");
				console.log(res);
				process.exit(1);
			}
			else{//otherwise, loop through results to find desired locationID for the location name
				var locations = res.locations;
				var locationID = "";
				for(i=0; i<locations.length; i++){
					if(locationName == locations[i].name){
						locationID = locations[i].locationId;
					}
				}
				if(locationID == "" || locationID == undefined ){//if location name not in the results, report error and exit
					console.log(locationName + " was not in the locations list.");
					process.exit(1);
				}
				else{//if device was found for location name, run DELETE device
					console.log("Deleting location...");
					apiLib.deleteLocation(locationID)
					.then(function(result){
						console.log(result);
					});
				}
			}
		});
	}
});