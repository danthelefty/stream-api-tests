/**********************
 * Script for getting the list of alerts for a specified location.
 * Takes in the user JSON and location name as command line arguments. I.e. "node getLocationAlerts.js myUser.json myHouse"
**********************/

//import modules
var apiLib = require("./RWC_API_lib.js");
var fs = require('fs');

//get command line arguments
var userFile = "";
var locationName = "";
if (process.argv.length < 4){
	console.log("Please pass a user JSON file and location name as an argumenst to this script. For example:\n   node getLocationAlerts.js myUser.json myHouse");
	process.exit(1);
}else{
	userFile = process.argv[2];
	locationName = process.argv[3];
}

//read in environment and user data from JSON files
try{
var envJSON = JSON.parse(fs.readFileSync('environment.json', 'utf8'));
var userJSON = JSON.parse(fs.readFileSync(userFile, 'utf8'));
}
catch(e){
	console.log("Error parsing JSON: " + e);
	process.exit(1);
}

//authenticate the user
apiLib.userAuth(envJSON, userJSON, function callback(success, err){
	if(success){//if authentication was successful
		apiLib.getAllLocations()
		.then(function(res){
			if("code" in res){//if error getting all locations, report error and exit
				console.log("Error getting all locations");
				console.log(res);
				process.exit(1);
			}
			else{//otherwise, loop through results to find desired locationID for the location name
				var locations = res.locations;
				var found = false;
				var alertsAray = [];
				for(i=0; i<locations.length; i++){
					if(locationName == locations[i].name){
						alertsAray = locations[i].alerts;
						found = true;
					}
				}
				if(found){
					console.log("Alerts for " + locationName + ":");
					console.log(JSON.stringify(alertsAray, null, 3));
				}
				else{
					console.log("The location " + locationName + " was not in the locations list for this account.");
				}
			}
		});
	}
});