/**********************
 * Script for posting a new location to the account.
 * Takes in a command line arguments for the user JSON, locationName, timezone, zipcode, and buildingtype. I.e. 'node postNewLocation.js user.json myHome -5 "12345" home'
**********************/

//import modules
var apiLib = require("./RWC_API_lib.js");
var fs = require('fs');

var userFile = "";
var locationName = "";
var timezone = 0;
var zipcode = "";
var buildingType = "";
if (process.argv.length < 7){
	console.log('Please pass a user JSON, locationName, timezone, zipcode, and buildingtype as arguments to this script. For example:\n   node postNewLocation.js user.json myHome -5 "12345" home');
	process.exit(1);
}else{
	userFile = process.argv[2];
	locationName = process.argv[3];
	timezone = process.argv[4];
	zipcode = process.argv[5];
	buildingType = process.argv[6];
}

//read in environment and user data from JSON files
try{
var envJSON = JSON.parse(fs.readFileSync('environment.json', 'utf8'));
var userJSON = JSON.parse(fs.readFileSync(userFile, 'utf8'));
}
catch(e){
	console.log("Error parsing JSON: " + e);
	process.exit(1);
}

//authenticate the user
apiLib.userAuth(envJSON, userJSON, function callback(success, err){
	if(success){//if authentication was successful
		apiLib.postLocation(locationName, timezone, zipcode, buildingType)
			.then(function (results){
				console.log(JSON.stringify(results, null, 3));
			});
	}
});